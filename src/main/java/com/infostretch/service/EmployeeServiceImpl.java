/**
 * 
 */
package com.infostretch.service;

import java.util.ArrayList;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.infostretch.dto.EmployeesDTO;
import com.infostretch.entity.Employees;
import com.infostretch.repository.EmployeeRepository;

/**
 * @author abhijit.raval
 *
 */
@Service
public class EmployeeServiceImpl implements EmployeeService {

	@Autowired
	EmployeeRepository employeeRepo;
	@Autowired
	ModelMapper mapper;

	@Override
	public void insertData(EmployeesDTO employeesDto) {
		// TODO Auto-generated method stub
		Employees employees = mapper.map(employeesDto, Employees.class);
		employeeRepo.save(employees);
	}

	@Override
	public List<EmployeesDTO> getAll() {
		// TODO Auto-generated method stub
		List<Employees> employees=employeeRepo.findAll();
		System.out.println(employees);
		List<EmployeesDTO> allEmployeesList = new ArrayList<EmployeesDTO>();
		for (Employees employees2 : employees) {
			EmployeesDTO dto=mapper.map(employees2, EmployeesDTO.class);
			allEmployeesList.add(dto);
		}
		return allEmployeesList;
	}

	@Override
	public void deleteEmployee(long id) {
		// TODO Auto-generated method stub
		employeeRepo.deleteById(id);
	}

	@Override
	public void updateEmployee(long id,EmployeesDTO updateDto) {
		// TODO Auto-generated method stub
		Employees employees=mapper.map(updateDto, Employees.class);
		employees.setId(id);
		employeeRepo.save(employees);
	}

}
