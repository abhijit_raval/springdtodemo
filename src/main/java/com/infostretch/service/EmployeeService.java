package com.infostretch.service;

import java.util.List;

import com.infostretch.dto.EmployeesDTO;
import com.infostretch.entity.Employees;

public interface EmployeeService {

	void insertData(EmployeesDTO employeesDto);
	List<EmployeesDTO> getAll();
	void deleteEmployee(long id);
	void updateEmployee(long id,EmployeesDTO updateDto);
}
