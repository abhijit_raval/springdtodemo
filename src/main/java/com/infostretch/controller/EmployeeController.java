package com.infostretch.controller;

import java.lang.reflect.InvocationTargetException;
import java.net.URI;
import java.util.List;

import org.apache.commons.beanutils.BeanUtils;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.infostretch.dto.EmployeesDTO;
import com.infostretch.entity.Employees;
import com.infostretch.service.EmployeeService;

@RestController
public class EmployeeController {

	@Autowired
	EmployeeService employeeService;

	@PostMapping("/employees")
	ResponseEntity<Object> addEmployee(@RequestBody EmployeesDTO employeesDto) {
		System.out.println("EmployeeDTO contact " + employeesDto.getMobileNumber());
		this.employeeService.insertData(employeesDto);
		return ResponseEntity.status(HttpStatus.CREATED).body(employeesDto);
	}

	@GetMapping("/employees")
	ResponseEntity<List<EmployeesDTO>> getAllEmployees() {
		return ResponseEntity.ok(employeeService.getAll());
	}

	@DeleteMapping("/employees/{id}")
	ResponseEntity deleteEmployee(@PathVariable long id) {
		this.employeeService.deleteEmployee(id);
		return ResponseEntity.status(HttpStatus.ACCEPTED).build();
	}

	@PutMapping("/employees/{id}")
	ResponseEntity<EmployeesDTO> updateEmployee(@PathVariable long id, @RequestBody EmployeesDTO updateDto) {
		this.employeeService.updateEmployee(id, updateDto);
		return ResponseEntity.status(HttpStatus.ACCEPTED).body(updateDto);
	}
}
