package com.infostretch.springboot;

import javax.persistence.Entity;

import org.modelmapper.ModelMapper;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication(scanBasePackages = { "com.infostretch" })
@EnableJpaRepositories("com.infostretch.repository")
@EntityScan("com.infostretch.entity")

public class EmployeeBootApplication {

	public static void main(String[] args) {
		SpringApplication.run(EmployeeBootApplication.class, args);
	}

	@Bean(name = "modelMapper")
	public ModelMapper getModelMapper() {
		ModelMapper modelMapper = new ModelMapper();
		return modelMapper;
	}
}
