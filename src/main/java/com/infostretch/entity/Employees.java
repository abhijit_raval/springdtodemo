package com.infostretch.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Type;

@Entity
public class Employees {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "emp_id")
	@Type(type = "java.lang.Long")
	private Long id;
	@Column(name = "emp_first_name")
	private String firstName;
	@Column(name = "emp_last_name")
	private String lastName;
	@Column(name = "emp_contact_number")
	@Type(type = "java.lang.Long")
	private Long mobileNumber;

	@Column(name = "emp_address")
	private String address;

	public Employees() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Employees(String firstName, String lastName, long mobieNumber, String address) {
		super();
		this.firstName = firstName;
		this.lastName = lastName;
		this.mobileNumber = mobieNumber;
		this.address = address;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public Long getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(Long mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	@Override
	public String toString() {
		return "Employees [id=" + id + ", firstName=" + firstName + ", lastName=" + lastName + ", mobieNumber="
				+ mobileNumber + ", address=" + address + "]";
	}

}
